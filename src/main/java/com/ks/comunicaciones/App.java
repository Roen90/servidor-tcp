package com.ks.comunicaciones;

import com.ks.configuracion;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        if (args.length >= 2){
            servidorTCP server = new servidorTCP();
            server.setPuerto( Integer.parseInt(args[0]));
            if (configuracion.getRuta().contains("\\")){
                server.setFile(configuracion.getRuta() + "transaction\\" + args[1]);
            } else {
                server.setFile(configuracion.getRuta() + "transaction/" + args[1]);
            }
            server.openFile();
            server.conectar();
            /*
            try {
                Thread.currentThread().sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            */
        }
    }
}
