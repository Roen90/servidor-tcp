package com.ks.comunicaciones;

import com.ks.tcp.Cliente;
import com.ks.tcp.EventosTCP;
import com.ks.tcp.Servidor;
import com.ks.tcp.Tcp;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Created by migue on 16/10/2015.
 */
public class servidorTCP  extends Servidor implements EventosTCP{
    private String VMstrFile;
    private BufferedReader VMioFile;
    private int VMintContador;

    public servidorTCP() {
        VMstrFile = "";
        VMintContador = 0;
        this.setEventos(this);
    }

    public void setFile(String file) {
        VMstrFile = file;
    }

    public void openFile() {
        try {
            VMioFile = new BufferedReader(new FileReader(VMstrFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void conexionEstablecida(Cliente cliente) {
        System.out.println("Se conecto con el servidor");
    }

    public void errorConexion(String s) {
        System.out.println("Problema al ponerse en escucha: " + s);
        System.exit(0);
    }

    public void datosRecibidos(String s, byte[] bytes, Tcp tcp) {
        VMintContador += 1;
        System.out.println("Mensaje recibido " + VMintContador);
        System.out.println(s);
        String line;
        try {
            if ((line = VMioFile.readLine()) != null) {
                System.out.println("Mensaje enviado " + VMintContador);
                System.out.println(line);
                this.enviar(obtenerLongitud(line.length() + 2) + line);
            } else {
                System.exit(0);
            }
        } catch (Exception ex) {
            System.out.println("Problema al mandar la transaccion: " + ex.getMessage());
        }
    }

    public void cerrarConexion(Cliente cliente) {

    }

    public static synchronized String obtenerLongitud(int Longitud) {
        NumberFormat VLobjFormat = new DecimalFormat("0000000000000000");
        String VLstrBinario = VLobjFormat.format(Double.parseDouble(Integer.toBinaryString(Longitud)));
        byte VLbyteDatos[] = new byte[2];
        VLbyteDatos[0] = (byte) Integer.parseInt(VLstrBinario.substring(0, 8), 2);
        VLbyteDatos[1] = (byte) Integer.parseInt(VLstrBinario.substring(8, 16), 2);
        return new String(VLbyteDatos);
    }
}
